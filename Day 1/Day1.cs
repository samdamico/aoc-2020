﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Advent
{
    static class Day1
    {
        public static void Go()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Projects\Advent\Day 1\Day1.txt");
            int[] nums = lines.Select(x => int.Parse(x)).ToArray();
            HashSet<int> found = new HashSet<int>();

            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    int numI = nums[i];
                    int numJ = nums[j];
                    int tar = 2020 - numI - numJ;
                    if (found.Contains(tar))
                    {
                        Console.WriteLine(numI * numJ * tar);
                        return;
                    }
                    found.Add(numI);
                }
            }
        }
    }
}
