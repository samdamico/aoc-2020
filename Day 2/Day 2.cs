﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Advent
{
    static class Day2
    {
        public static void Go()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Projects\Advent\Day 2\Day2.txt");
            int validPasswords = 0;

            for(var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                var words = line.Split(' ');
                int low = int.Parse(words[0].Split('-')[0]);
                int high = int.Parse(words[0].Split('-')[1]);

                char target = words[1][0];

                int found = words[2].Count(x => x == target);
                if (found >= low && found <= high) validPasswords++;

            }

            Console.WriteLine(validPasswords);
        }

        public static void Go_Part2()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Projects\Advent\Day 2\Day2.txt");
            int validPasswords = 0;

            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                var words = line.Split(' ');
                int low = int.Parse(words[0].Split('-')[0]) -1;
                int high = int.Parse(words[0].Split('-')[1]) -1;

                char target = words[1][0];

                if (words[2][low] == target ^ words[2][high] == target) validPasswords++;

            }

            Console.WriteLine(validPasswords);
        }
    }
}
